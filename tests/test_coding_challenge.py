
from components.home.cookies import Cookies
from components.home.cookies import _Selectors as CookiesSelectors
from page_object_models.choose_your_seats import ChooseYourSeats
from page_object_models.home import Home
from page_object_models.trip import Trip
from utils.dates import short_date, string_to_timestamp
from utils.logger import logger
from utils.wait_for import wait_for_url


def test_coding_challenge(driver):
    logger.info("Starting coding challenge")
    flight_details = {
        "ryanair": "https://www.ryanair.com/",
        "flights": "https://www.ryanair.com/ie/en/trip/flights/",
        "seats": "https://www.ryanair.com/ie/en/trip/flights/seats",
        "bags": "https://www.ryanair.com/ie/en/trip/flights/bags",
        "origin-country": "Ireland",
        "origin-airport": "Dublin",
        "destination-country": "France",
        "destination-airport": "Marseille",
        "number-passengers": 2,
        "passengers": [
            {
                "title": "Mrs",
                "first_name": "Jane",
                "last_name": "Doe",
                "assistance": False
            },
            {
                "title": "Mr",
                "first_name": "John",
                "last_name": "Doe",
                "assistance": False
            }
        ]
    }
    # Step 1: Navigate to Ryanair website -> The Ryanair homepage is loaded.
    driver.get(flight_details["ryanair"])
    Cookies(parent=driver, selector=CookiesSelectors.POPUP, name="Cookies Popup").agree.click()

    # Ryanair auto redirects to "https://www.ryanair.com/ie/en", using regular expression to match expected URL.
    wait_for_url(driver=driver, url=flight_details["ryanair"])
    assert flight_details["ryanair"] in driver.current_url

    # Step 2: Search for a return flight -> The page with suggested flight is loaded,
    # with the correct dates and number of passengers.
    home = Home(driver=driver)
    home.search_widget.origin.click()
    assert home.origin_selector.displayed
    home.origin_selector.select_country(flight_details["origin-country"])
    home.origin_selector.select_airport(flight_details["origin-airport"])
    assert home.destination_selector.displayed
    home.destination_selector.select_country(flight_details["destination-country"])
    home.destination_selector.select_airport(flight_details["destination-airport"])

    assert home.date_selector_from.displayed

    departure_date = home.date_selector_from.left_calendar.available_dates[1]
    flight_details["departure-month"] = home.date_selector_from.left_month.value
    flight_details["departure-date"] = departure_date.value
    departure_date.click()

    assert home.date_selector_to.displayed
    return_date = home.date_selector_to.right_calendar.available_dates[0]
    flight_details["return-month"] = home.date_selector_to.right_month.value
    flight_details["return-date"] = return_date.value
    return_date.click()

    assert home.passenger_selector.displayed
    home.passenger_selector.adults.set_number_passengers(flight_details["number-passengers"])

    home.search_widget.search.click()

    wait_for_url(driver=driver, url=flight_details["flights"])
    assert flight_details["flights"] in driver.current_url

    trip = Trip(driver=driver)

    assert trip.trip_details_header.origin.displayed
    assert trip.trip_details_header.origin.value == flight_details["origin-airport"]
    assert trip.trip_details_header.destination.displayed
    assert trip.trip_details_header.destination.value == flight_details["destination-airport"]
    assert short_date(
        string_to_timestamp(
            day=flight_details["departure-date"],
            month=flight_details["departure-month"].split(" ")[0],
            year=flight_details["departure-month"].split(" ")[1]
        )
    ) + " - " + short_date(
        string_to_timestamp(
            day=flight_details["return-date"],
            month=flight_details["return-month"].split(" ")[0],
            year=flight_details["return-month"].split(" ")[1]
        )
    ) == trip.trip_details_header.dates
    assert trip.trip_details_header.passengers == flight_details["number-passengers"]

    # Step 3: Select suggested flights -> "Log in to myRyanair" and "Passengers" sections are displayed,
    # "Passengers" is disabled.
    assert trip.outbound.displayed
    trip.outbound.flight_cards[0].select.click()
    assert trip.inbound.displayed
    trip.inbound.flight_cards[0].select.click()

    assert trip.fares.displayed
    trip.fares.fare_cards[1].select.click()
    assert trip.log_in_widget.displayed
    assert trip.passengers.displayed
    assert not trip.passengers.enabled

    # Select "Log in later" -> "Passengers" section becomes active.
    trip.log_in_widget.log_in_later.click()
    assert trip.passengers.enabled

    # Enter valid values in the fields for the "Passengers" section and select "Continue" ->
    # "Where would you like to sit" page is displayed.
    trip.passengers.passenger_cards[0].enter_passenger_details(**flight_details["passengers"][0])
    trip.passengers.passenger_cards[1].enter_passenger_details(**flight_details["passengers"][1])

    trip.continue_b.click()

    wait_for_url(driver=driver, url=flight_details["seats"])
    assert flight_details["seats"] in driver.current_url

    # Choose available seats for the first flight of the journey and select "Next Flight" ->
    # Where would you like to sit for the second flight is displayed.
    seats = ChooseYourSeats(driver=driver)

    assert seats.seat_map.displayed
    assert seats.seats_info.flight.value == \
           f'{flight_details["origin-airport"]} to {flight_details["destination-airport"]}'
    seats.select_available_seat()
    seats.select_available_seat()
    seats.seats_info.next_flight.click()

    # Choose available seats for the return flight and select "Continue" -> "What bags are you taking" is displayed.
    # NOTE: Any popups displayed at this point will be skipped, e.g. "Fast Track".
    if seats.seat_map.seats_prompt.displayed:
        seats.seat_map.seats_prompt.no_thanks.click()
    assert seats.seat_map.displayed
    assert seats.seats_info.flight.value == \
           f'{flight_details["destination-airport"]} to {flight_details["origin-airport"]}'
    seats.select_available_seat()
    seats.select_available_seat()
    seats.seats_info.next.click()

    if seats.fast_track_popup.displayed:
        seats.fast_track_popup.no_thanks.click()
    wait_for_url(driver=driver, url=flight_details["bags"])
    assert flight_details["bags"] in driver.current_url

    logger.info("Test Passed")
