import pytest

from utils.driver import get_webdriver


@pytest.fixture(scope="session")
def driver():
    d = get_webdriver()
    yield d
    d.quit()
