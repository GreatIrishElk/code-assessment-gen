from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Text
from utils import Selector


class _JourneySelectors:
    TITLE = (By.CLASS_NAME, "header__title")
    CAROUSEL = (By.CLASS_NAME, "carousel__container")
    FLIGHT = (By.CLASS_NAME, "flight-card")


class _CarouselSelectors:
    DATE_ITEM = (By.CLASS_NAME, "date-item")
    PREVIOUS = (By.CLASS_NAME, "carousel-prev")
    NEXT = (By.CLASS_NAME, "carousel-next")


class _DateItemSelectors:
    DATE = (By.CLASS_NAME, "date-item__day-of-month")
    DAY = (By.CLASS_NAME, "date-item__day-of-week ")
    PRICE = (By.CLASS_NAME, "date-item__price")


class _FlightCardSelectors:
    TIME = (By.CLASS_NAME, "time")
    DURATION = (By.CLASS_NAME, "duration")
    FLIGHT_NUMBER = (By.CLASS_NAME, "card-flight-num__content")
    TYPE = (By.CLASS_NAME, "card-info__label")
    SEATS_LEFT = (By.CLASS_NAME, "price-fares-left")
    PRICE = (By.CLASS_NAME, "price-value")
    SELECT = (By.CLASS_NAME, "card-price__button")


class _DateItem(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_DateItem, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _DateItemSelectors()

    @property
    def date(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.DATE,
            name="Date"
        )

    @property
    def day(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.DAY,
            name="Day"
        )

    @property
    def price(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.PRICE,
            name="Price"
        )

    @property
    def enabled(self) -> bool:
        return "date-item--disabled" in self.element.get_attribute("class")


class _Carousel(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_Carousel, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _CarouselSelectors()

    @property
    def previous(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.PREVIOUS,
            name="Previous"
        )

    @property
    def next(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.NEXT,
            name="Next"
        )

    @property
    def _date_items(self) -> [_DateItem]:
        date_items = []
        for date_item in self.element.find_elements(*self._selectors.DATE_ITEM):
            date_items.append(
                _DateItem(
                    parent=self.element,
                    selector=date_item,
                    name="Date Item"
                )
            )
        return date_items

    @property
    def date_item_one(self) -> _DateItem:
        return self._date_items[0]

    @property
    def date_item_two(self) -> _DateItem:
        return self._date_items[1]

    @property
    def date_item_three(self) -> _DateItem:
        return self._date_items[2]

    @property
    def date_item_four(self) -> _DateItem:
        return self._date_items[3]

    @property
    def date_item_five(self) -> _DateItem:
        return self._date_items[4]


class _FlightCard(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_FlightCard, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _FlightCardSelectors()

    @property
    def _times(self) -> [Text]:
        times = []
        for t in self.element.find_elements(*self._selectors.TIME):
            times.append(
                Text(
                    parent=self.element,
                    selector=t,
                    name="Time"
                )
            )
        return times

    @property
    def departure(self) -> Text:
        return self._times[0]

    @property
    def set_down(self) -> Text:
        return self._times[1]

    @property
    def duration(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.DURATION,
            name="Duration"
        )

    @property
    def flight_number(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.FLIGHT_NUMBER,
            name="Flight Number"
        )

    @property
    def type(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.TYPE,
            name="Type"
        )

    @property
    def seats_left(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.SEATS_LEFT,
            name="Seats Left"
        )

    @property
    def price(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.PRICE,
            name="Price"
        )

    @property
    def select(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.SELECT,
            name="Select"
        )


class Journey(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(Journey, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _JourneySelectors()

    @property
    def title(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.TITLE,
            name="Title"
        )

    @property
    def carousel(self) -> _Carousel:
        return _Carousel(
            parent=self.element,
            selector=self._selectors.CAROUSEL,
            name="Flights Carousel"
        )

    @property
    def flight_cards(self) -> [_FlightCard]:
        flights = []
        for flight in self.element.find_elements(*self._selectors.FLIGHT):
            flights.append(
                _FlightCard(
                    parent=self.element,
                    selector=flight,
                    name="Flight"
                )
            )
        return flights
