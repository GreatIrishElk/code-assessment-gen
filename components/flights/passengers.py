from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import (BaseComponent, DropdownMenu, Text,
                                          TextField, ToggleButton)
from utils import Selector


class _PassengersSelectors:
    PASSENGER_CARD = (By.TAG_NAME, "pax-passenger-container")


class _PassengersCardSelectors:
    HEADER = (By.CLASS_NAME, "passenger__header")
    TITLE = (By.CLASS_NAME, "dropdown__toggle")
    DROPDOWN = (By.CLASS_NAME, "dropdown")
    DROPDOWN_LIST = (By.CLASS_NAME, "dropdown__menu")
    DROPDOWN_ITEM = (By.CLASS_NAME, "dropdown-item__link")
    NAME = (By.CLASS_NAME, "invisible-background")
    NEED_SPECIAL_ASSISTANCE = (By.CLASS_NAME, "toggle-button__bar")


class _PassengerCard(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_PassengerCard, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _PassengersCardSelectors()

    @property
    def header(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.HEADER,
            name="Header"
        )

    @property
    def title(self) -> DropdownMenu:
        return DropdownMenu(
            parent=self.element,
            selector=self._selectors.DROPDOWN,
            name="Title",
            dropdown_list=self._selectors.DROPDOWN_LIST,
            dropdown_item=self._selectors.DROPDOWN_ITEM
        )

    @property
    def _names(self) -> [TextField]:
        names = []
        for name in self.element.find_elements(*self._selectors.NAME):
            names.append(
                TextField(
                    parent=self.element,
                    selector=name,
                    name="Name"
                )
            )
        return names

    @property
    def first_name(self) -> TextField:
        return self._names[0]

    @property
    def last_name(self) -> TextField:
        return self._names[1]

    @property
    def need_assistance(self) -> ToggleButton:
        return ToggleButton(
            parent=self.element,
            selector=self._selectors.NEED_SPECIAL_ASSISTANCE,
            name="Need Special Assistance"
        )

    def enter_passenger_details(self, title: str, first_name: str, last_name: str, assistance: bool):
        self.title.set(title),
        self.first_name.value = first_name,
        self.last_name.value = last_name,
        self.need_assistance.on = assistance


class Passengers(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(Passengers, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _PassengersSelectors()

    @property
    def passenger_cards(self) -> [_PassengerCard]:
        cards = []
        for card in self.element.find_elements(*self._selectors.PASSENGER_CARD):
            cards.append(
                _PassengerCard(
                    parent=self.element,
                    selector=card,
                    name="Passenger Card"
                )
            )
        return cards

    @property
    def enabled(self) -> bool:
        return "form-wrapper--disabled" not in self.element.get_attribute("class")
