from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Text
from utils import Selector


class _Selectors:
    TITLE = (By.CLASS_NAME, "login-touchpoint__title")
    SUB_TITLE = (By.CLASS_NAME, "login-touchpoint__subheader")
    POINTS = (By.CLASS_NAME, "login-touchpoint__points")
    SIGN_UP = (By.CLASS_NAME, "login-touchpoint__signup-button")
    LOG_IN = (By.CLASS_NAME, "login-touchpoint__login-button")
    LOG_IN_LATER = (By.CLASS_NAME, "login-touchpoint__expansion-bar")


class LogInWidget(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(LogInWidget, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _Selectors()

    @property
    def title(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.TITLE,
            name="Title"
        )

    @property
    def sub_title(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.SUB_TITLE,
            name="Sub Title"
        )

    @property
    def points(self) -> [Text]:
        return Text(
            parent=self.element,
            selector=self._selectors.POINTS,
            name="Points"
        )

    @property
    def sign_up(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.SIGN_UP,
            name="Sign Up"
        )

    @property
    def log_in(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.LOG_IN,
            name="Log In"
        )

    @property
    def log_in_later(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.LOG_IN_LATER,
            name="Log In Later"
        )
