import re

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Text
from utils import Selector


class _Selectors:
    DETAILS_HEADER = (By.CLASS_NAME, "details__header")
    PLACE = (By.TAG_NAME, "h4")
    EDIT_SEARCH = (By.CLASS_NAME, "details__edit-search")
    DETAILS_BOTTOM_BAR = (By.CLASS_NAME, "details__bottom-bar")


class FlightDetails(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(FlightDetails, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _Selectors()

    @property
    def _details_header(self) -> WebElement:
        return self.element.find_element(*self._selectors.DETAILS_HEADER)

    @property
    def origin(self) -> Text:
        return Text(
            parent=self.element,
            selector=self.element.find_elements(*self._selectors.PLACE)[0],
            name="Origin"
        )

    @property
    def destination(self) -> Text:
        return Text(
            parent=self.element,
            selector=self.element.find_elements(*self._selectors.PLACE)[1],
            name="Destination"
        )

    @property
    def edit_search(self) -> Button:
        return Button(
            parent=self._details_header,
            selector=self._selectors.EDIT_SEARCH,
            name="Edit Search"
        )

    @property
    def details_bottom_bar(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.DETAILS_BOTTOM_BAR,
            name="Details Bottom Bar"
        )

    @property
    def _details_bottom_bar_split(self) -> [str]:
        return re.split(r"\n((?!\s))", self.details_bottom_bar.value)

    @property
    def trip_type(self) -> str:
        return self._details_bottom_bar_split[0]

    @property
    def dates(self) -> str:
        return re.sub("\n", "", self._details_bottom_bar_split[2])

    @property
    def passengers(self) -> int:
        return int(self._details_bottom_bar_split[4])
