from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Image, Text
from utils import Selector


class _FaresSelectors:
    TITLE = (By.CLASS_NAME, "fare-selector__title")
    MESSAGE = (By.CLASS_NAME, "journey-message")
    FARE_CARD = (By.CLASS_NAME, "fare-card-item--full-height")


class _FaresCardSelectors:
    TITLE = (By.CLASS_NAME, "fare-card__title")
    SUB_TITLE = (By.CLASS_NAME, "subtitle-m-lg")
    BENEFIT_ITEM = (By.CLASS_NAME, "benefits__item")
    BUTTON = (By.CLASS_NAME, "fare-card__button")


class _BenefitSelectors:
    ICON = (By.CLASS_NAME, "benefits__icon")
    DESCRIPTION = (By.TAG_NAME, "div")


class _Benefit(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_Benefit, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _BenefitSelectors()

    @property
    def icon(self) -> Image:
        return Image(
            parent=self.element,
            selector=self._selectors.ICON,
            name="Icon"
        )

    @property
    def _description(self) -> Text:
        return Text(
            parent=self.element,
            selector=self.element.find_elements(*self._selectors.DESCRIPTION)[1],
            name="Description"
        )

    @property
    def description_header(self) -> str:
        return self._description.value.split("\n")[0]

    @property
    def description_info(self) -> str:
        return self._description.value.split("\n")[1]


class _FareCard(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_FareCard, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _FaresCardSelectors()

    @property
    def title(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.TITLE,
            name="Title"
        )

    @property
    def sub_title(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.SUB_TITLE,
            name="Sub Title"
        )

    @property
    def benefits(self) -> [_Benefit]:
        benefits = []
        for benefit in self.element.find_elements(*self._selectors.BENEFIT_ITEM):
            benefits.append(
                _Benefit(
                    parent=self.element,
                    selector=benefit,
                    name="Benefit"
                )
            )
        return benefits

    @property
    def select(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.BUTTON,
            name="Select"
        )


class Fares(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(Fares, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _FaresSelectors()

    @property
    def fare_cards(self) -> [_FareCard]:
        fare_cards = []
        for fare_card in self.element.find_elements(*self._selectors.FARE_CARD):
            if "ng-star-inserted" in fare_card.get_attribute("class"):
                fare_cards.append(
                    _FareCard(
                        parent=self.element,
                        selector=fare_card,
                        name="Fare Card"
                    )
                )
        return fare_cards
