from components.flights.fares import Fares
from components.flights.flight_details import FlightDetails
from components.flights.journey import Journey
from components.flights.log_in_widget import LogInWidget
from components.flights.passengers import Passengers
