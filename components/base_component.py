import time
from abc import ABC

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from utils import Selector
from utils.config import config
from utils.logger import logger


class BaseComponent(ABC):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        self._parent = parent
        self._name = name
        self.element = self._to_element(selector)

    def _to_element(self, value: Selector | WebElement) -> WebElement | None:
        match value:
            case (str(), str()) as selector:
                try:
                    return self._parent.find_element(*selector)
                except NoSuchElementException:
                    return None
            case WebElement() as element:
                return element
            case _:
                logger.error("Element not found")
                raise NotImplemented

    @property
    def _driver(self):
        if isinstance(self._parent, WebDriver):
            return self._parent
        return self._parent.parent

    @property
    def displayed(self, wait: int = config.wait) -> bool:
        logger.info("Checking if %s is displayed", self._name)
        try:
            if self.element:
                waited = 0
                while not self.element.is_displayed():
                    if waited > wait:
                        break
                    time.sleep(1)
                    waited += 1
                return self.element.is_displayed()
            return False
        except NoSuchElementException:
            return False

    @property
    def enabled(self) -> bool:
        logger.info("Checking if %s is enabled", self._name)
        try:
            return self.element.is_enabled()
        except NoSuchElementException:
            return False
