from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Link, Text
from utils import Selector


class _Selectors:
    POPUP = (By.CLASS_NAME, "cookie-popup-with-overlay__box")
    TITLE = (By.CLASS_NAME, "cookie-popup-with-overlay__title")
    TEXT = (By.TAG_NAME, "p")
    PRIVACY_POLICY = (By.CLASS_NAME, "cookie-policy__privacy-link")
    COOKIE_POLICY = (By.PARTIAL_LINK_TEXT, "Cookie Policy")
    VIEW_COOKIE_SETTINGS = (By.CLASS_NAME, "cookie-popup-with-overlay__button-settings")
    AGREE = (By.CLASS_NAME, "cookie-popup-with-overlay__button")


class Cookies(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(Cookies, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _Selectors()

    @property
    def title(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.TITLE,
            name="Cookies Title"
        )

    @property
    def text(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.TEXT,
            name="Cookies Text"
        )

    @property
    def privacy_policy(self) -> Link:
        return Link(
            parent=self.element,
            selector=self._selectors.PRIVACY_POLICY,
            name="Privacy Policy"
        )

    @property
    def cookies_policy(self) -> Link:
        return Link(
            parent=self.element,
            selector=self.text.element.find_element(*self._selectors.COOKIE_POLICY),
            name="Cookies Policy"
        )

    @property
    def view_cookie_settings(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.VIEW_COOKIE_SETTINGS,
            name="View Cookie Settings"
        )

    @property
    def agree(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.AGREE,
            name="Yes, I agree"
        )
