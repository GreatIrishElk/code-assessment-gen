import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Text
from utils import Selector
from utils.config import config
from utils.logger import logger


class _DateSelectorSelectors:
    TAB_TEXT = (By.CLASS_NAME, "tab-text")
    MONTH = (By.CLASS_NAME, "m-toggle__scrollable-item")
    TOGGLE_BUTTON = (By.CLASS_NAME, "m-toggle__button")
    MONTH_NAME = (By.CLASS_NAME, "calendar__month-name")
    CALENDAR = (By.CLASS_NAME, "datepicker__calendar ")


class _CalendarSelectors:
    WEEKDAY = (By.CLASS_NAME, "calendar-body__weekday")
    DATE = (By.CLASS_NAME, "calendar-body__cell")


class _Calendar(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_Calendar, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _CalendarSelectors()

    @property
    def _weekdays(self) -> [WebElement]:
        return self.element.find_elements(*self._selectors.WEEKDAY)

    @property
    def _dates(self) -> [Text]:
        dates = []
        for date in self.element.find_elements(*self._selectors.DATE):
            dates.append(
                Text(
                    parent=self.element,
                    selector=date,
                    name=date.text
                )
            )
        return dates

    @staticmethod
    def _date_available(date: Text) -> bool:
        if "calendar-body__cell--disabled" in date.element.get_attribute("class"):
            return False
        return True

    @property
    def available_dates(self) -> [Text]:
        enabled = []
        for date in self._dates:
            if self._date_available(date):
                enabled.append(date)
        return enabled

    @property
    def unavailable_dates(self) -> [Text]:
        not_enabled = []
        for date in self._dates:
            if not self._date_available(date):
                not_enabled.append(date)
        return not_enabled

    @property
    def monday(self) -> Text:
        return self._weekdays[0]

    @property
    def tuesday(self) -> Text:
        return self._weekdays[1]

    @property
    def wednesday(self) -> Text:
        return self._weekdays[2]

    @property
    def thursday(self) -> Text:
        return self._weekdays[3]

    @property
    def friday(self) -> Text:
        return self._weekdays[4]

    @property
    def saturday(self) -> Text:
        return self._weekdays[5]

    @property
    def sunday(self) -> Text:
        return self._weekdays[6]

    def select_date(self, option: str):
        for date in self._dates:
            if date.value == option:
                date.click()
                break
        else:
            raise NoSuchElementException


class _DatesSelector(BaseComponent):
    DATE_REF = None

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_DatesSelector, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _DateSelectorSelectors()

    @property
    def _tabs(self) -> [WebElement]:
        return self.element.find_elements(*self._selectors.TAB_TEXT)

    @property
    def exact_dates(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._tabs[0],
            name="Exact dates"
        )

    @property
    def flexible_dates(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._tabs[1],
            name="Flexible dates"
        )

    @property
    def _months(self) -> [WebElement]:
        return self.element.find_elements(*self._selectors.MONTH_NAME)

    @property
    def left_month(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._months[0],
            name=self._months[0].text
        )

    @property
    def right_month(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._months[1],
            name=self._months[1].text
        )

    @property
    def _calendars(self) -> [WebElement]:
        return self.element.find_elements(*self._selectors.CALENDAR)

    @property
    def left_calendar(self) -> _Calendar:
        return _Calendar(
            parent=self.element,
            selector=self._calendars[0],
            name=self.left_month.value
        )

    @property
    def right_calendar(self) -> _Calendar:
        return _Calendar(
            parent=self.element,
            selector=self._calendars[1],
            name=self.right_month.value
        )

    @property
    def displayed(self, wait: int = config.wait) -> bool:
        logger.info("Checking if %s is displayed", self._name)
        try:
            if self.element:
                waited = 0
                while not self.element.is_displayed() or self.element.get_attribute("data-ref") != self.DATE_REF:
                    if waited > wait:
                        break
                    time.sleep(1)
                    waited += 1
                return self.element.is_displayed() and self.element.get_attribute("data-ref") == self.DATE_REF
            return False
        except NoSuchElementException:
            return False


class DatePickerFrom(_DatesSelector):
    DATE_REF = "fsw-datepicker-container__from"


class DatePickerTo(_DatesSelector):
    DATE_REF = "fsw-datepicker-container__to"
