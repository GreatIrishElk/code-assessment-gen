from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, TextField
from utils import Selector


class _Selectors:
    FLIGHTS = (By.CLASS_NAME, "search-widget-tabs__flights")
    TRIP_TYPE = (By.CLASS_NAME, "trip-type__button")
    INPUT = (By.CLASS_NAME, "input-button__input")
    SEARCH = (By.CLASS_NAME, "flight-search-widget__start-search")


class SearchWidget(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(SearchWidget, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _Selectors()

    @property
    def flights(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.FLIGHTS,
            name="Flights"
        )

    @property
    def return_trip(self) -> Button:
        return Button(
            parent=self.element,
            selector=self.element.find_elements(*self._selectors.TRIP_TYPE)[0],
            name="Return Trip"
        )

    @property
    def one_way(self) -> Button:
        return Button(
            parent=self.element,
            selector=self.element.find_elements(*self._selectors.TRIP_TYPE)[1],
            name="One Way"
        )

    @property
    def _inputs(self) -> [WebElement]:
        return self.element.find_elements(*self._selectors.INPUT)

    @property
    def origin(self) -> TextField:
        return TextField(
            parent=self.element,
            selector=self._inputs[0],
            name="From"
        )

    @property
    def destination(self) -> TextField:
        return TextField(
            parent=self.element,
            selector=self._inputs[1],
            name="Destination"
        )

    @property
    def depart_date(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._inputs[2],
            name="Depart date"
        )

    @property
    def return_date(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._inputs[3],
            name="Return date"
        )

    @property
    def passengers(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._inputs[4],
            name="Passengers"
        )

    @property
    def search(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.SEARCH,
            name="Search"
        )
