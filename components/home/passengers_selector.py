from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Text
from utils import Selector


class _PassengerSelectorSelectors:
    ALERT = (By.CLASS_NAME, "alert__wrapper ")
    PASSENGER_COUNTER = (By.CLASS_NAME, "passengers-picker__passenger-type")


class _PassengerCounterSelectors:
    LABEL = (By.CLASS_NAME, "passengers-picker__counter-main-label")
    SUB_LABEL = (By.CLASS_NAME, "passengers-picker__counter-sub-label")
    COUNTERS = (By.CLASS_NAME, "counter")
    COUNTER = (By.TAG_NAME, "div")
    NUMBER_OF_PASSENGERS = (By.CLASS_NAME, "counter__value")


class _PassengerCounter(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector, name: str):
        super(_PassengerCounter, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _PassengerCounterSelectors()

    @property
    def label(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.LABEL,
            name="Label"
        )

    @property
    def sub_label(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.SUB_LABEL,
            name="Sub Label"
        )

    @property
    def number_of_passengers(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.NUMBER_OF_PASSENGERS,
            name=f"Number of {self.label.value} Passengers"
        )

    @property
    def _counters(self) -> WebElement:
        return self.element.find_element(*self._selectors.COUNTERS)

    @property
    def _counter(self) -> [WebElement]:
        return self._counters.find_elements(*self._selectors.COUNTER)

    @property
    def minus(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._counter[0],
            name="Minus"
        )

    @property
    def plus(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._counter[3],
            name="Plus"
        )

    def set_number_passengers(self, value: int):
        if int(self.number_of_passengers.value) == value:
            return
        while int(self.number_of_passengers.value) < value:
            self.plus.click()
            return
        while int(self.number_of_passengers.value) > value:
            self.minus.click()
            return


class PassengersSelector(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(PassengersSelector, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _PassengerSelectorSelectors()

    @property
    def alert(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.ALERT,
            name="Alert"
        )

    @property
    def _passenger_counters(self) -> [WebElement]:
        return self.element.find_elements(*self._selectors.PASSENGER_COUNTER)

    @property
    def adults(self) -> _PassengerCounter:
        return _PassengerCounter(
            parent=self.element,
            selector=self._passenger_counters[0],
            name="Adults Counter"
        )

    @property
    def teens(self) -> _PassengerCounter:
        return _PassengerCounter(
            parent=self.element,
            selector=self._passenger_counters[1],
            name="Teens Counter"
        )

    @property
    def children(self) -> _PassengerCounter:
        return _PassengerCounter(
            parent=self.element,
            selector=self._passenger_counters[2],
            name="Children Counter"
        )

    @property
    def infants(self) -> _PassengerCounter:
        return _PassengerCounter(
            parent=self.element,
            selector=self._passenger_counters[3],
            name="Infants Counter"
        )
