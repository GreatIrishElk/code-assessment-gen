import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Text
from utils import Selector
from utils.config import config
from utils.logger import logger


class _Selectors:
    TITLE = (By.CLASS_NAME, "title-s-lg")
    COUNTRY = (By.CLASS_NAME, "countries__country")
    PICK_AN_AIRPORT = (By.CLASS_NAME, "list__header-title")
    AIRPORT = (By.CLASS_NAME, "airport-item")
    CLEAR_SELECTION = (By.CLASS_NAME, "list__clear-selection")


class _DestinationSelectors(_Selectors):
    DESTINATION_CONTAINER = (By.TAG_NAME, "fsw-destination-tabs-container")


class _DestinationTabsContainerSelectors:
    OPTION = (By.CLASS_NAME, "destination-tabs__button")


class _FlightSelector(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_FlightSelector, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _Selectors()

    @property
    def title(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.TITLE,
            name="Title"
        )

    @property
    def countries(self) -> [Button]:
        return self._get_countries()

    def _get_countries(self) -> [Button]:
        countries = []
        for country in self.element.find_elements(*self._selectors.COUNTRY):
            countries.append(
                Button(
                    parent=self.element,
                    selector=country,
                    name=country.text
                )
            )
        return countries

    def select_country(self, option: str):
        for country in self.countries:
            if country.value == option:
                country.click()
                break
        else:
            raise NoSuchElementException

    @property
    def pick_an_airport(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.PICK_AN_AIRPORT,
            name="Pick an Airport"
        )

    @property
    def airports(self) -> [Button]:
        return self._get_airports()

    def _get_airports(self) -> [Button]:
        airports = []
        for airport in self.element.find_elements(*self._selectors.AIRPORT):
            airports.append(
                Button(
                    parent=self.element,
                    selector=airport,
                    name=airport.text
                )
            )
        return airports

    def select_airport(self, option: str):
        for airport in self.airports:
            if airport.value == option:
                airport.click()
                break
        else:
            raise NoSuchElementException

    @property
    def clear_selection(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.CLEAR_SELECTION,
            name="Clear Selection"
        )


class OriginSelector(_FlightSelector):

    @property
    def displayed(self, wait: int = config.wait) -> bool:
        logger.info("Checking if %s is displayed", self._name)
        try:
            if self.element:
                waited = 0
                while not self.element.is_displayed() or self.title.value != "Origin country":
                    if waited > wait:
                        break
                    time.sleep(1)
                    waited += 1
                return self.element.is_displayed() and self.title.value == "Origin country"
            return False
        except NoSuchElementException:
            return False


class _DestinationTabsContainer(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_DestinationTabsContainer, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _DestinationTabsContainerSelectors()

    @property
    def tabs(self) -> [WebElement]:
        return self.element.find_elements(*self._selectors.OPTION)


class DestinationSelector(_FlightSelector):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(DestinationSelector, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _DestinationSelectors()

    @property
    def _destinations_tabs_container(self) -> _DestinationTabsContainer:
        return _DestinationTabsContainer(
            parent=self.element,
            selector=self._selectors.DESTINATION_CONTAINER,
            name="Destinations Tabs Container"
        )

    @property
    def destination_or_country(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._destinations_tabs_container.tabs[0],
            name="Destination or Country"
        )

    @property
    def regions(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._destinations_tabs_container.tabs[1],
            name="Regions"
        )

    @property
    def popular_attractions(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._destinations_tabs_container.tabs[2],
            name="Popular Attractions"
        )

    @property
    def displayed(self, wait: int = config.wait) -> bool:
        logger.info("Checking if %s is displayed", self._name)
        try:
            if self.element:
                waited = 0
                while not self.element.is_displayed() or not self._destinations_tabs_container.displayed:
                    if waited > wait:
                        break
                    time.sleep(1)
                    waited += 1
                return self.element.is_displayed() and self._destinations_tabs_container.displayed
            return False
        except NoSuchElementException:
            return False
