from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Text
from utils import Selector


class _Selectors:
    BREADCRUMB = (By.CLASS_NAME, "breadcrumb__element ")


class _Breadcrumb(Text):

    def enabled(self) -> bool:
        return "breadcrumb__element--active" in self.element.get_attribute("class")


class FlightBreadCrumbs(BaseComponent):

    def __init__(self, parent: WebDriver, selector: Selector | WebElement, name: str):
        super(FlightBreadCrumbs, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _Selectors()

    @property
    def _breadcrumbs(self) -> [_Breadcrumb]:
        breadcrumbs = []
        for breadcrumb in self.element.find_elements(*self._selectors.BREADCRUMB):
            breadcrumbs.append(
                _Breadcrumb(
                    parent=self.element,
                    selector=breadcrumb,
                    name=breadcrumb.text
                )
            )
        return breadcrumbs

    @property
    def flights(self) -> _Breadcrumb:
        return self._breadcrumbs[0]

    @property
    def seats(self) -> _Breadcrumb:
        return self._breadcrumbs[1]

    @property
    def bags(self) -> _Breadcrumb:
        return self._breadcrumbs[2]

    @property
    def extras(self) -> _Breadcrumb:
        return self._breadcrumbs[3]

    @property
    def active_breadcrumb(self) -> _Breadcrumb | None:
        for breadcrumb in self._breadcrumbs:
            if breadcrumb.enabled():
                return breadcrumb
        return None
