from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Image, Text
from utils import Selector


class _Selector:
    PRICE = (By.CLASS_NAME, "price")
    BUTTONS_AREA = (By.CLASS_NAME, "enhanced-takeover-beta__product-actions")
    BUTTON = (By.TAG_NAME, "Button")


class FastTrackPopUp(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(FastTrackPopUp, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _Selector()

    @property
    def price(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.PRICE,
            name="Price"
        )

    @property
    def _buttons_area(self) -> WebElement:
        return self.element.find_element(*self._selectors.BUTTONS_AREA)

    @property
    def _buttons(self) -> [WebElement]:
        return self._buttons_area.find_elements(*self._selectors.BUTTON)

    @property
    def no_thanks(self) -> Button:
        return Button(
            parent=self._buttons_area,
            selector=self._buttons[0],
            name="No, Thanks"
        )

    @property
    def add_fast_track(self) -> Button:
        return Button(
            parent=self._buttons_area,
            selector=self._buttons[1],
            name="Add Fast Track"
        )
