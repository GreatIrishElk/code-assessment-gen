from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Text
from utils import Selector


class _Selectors:
    FLIGHT = (By.CLASS_NAME, "passenger-carousel__header-direction")
    BUTTON = (By.CLASS_NAME, "seats-action__button")


class SeatsInfo(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(SeatsInfo, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _Selectors()

    @property
    def flight(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.FLIGHT,
            name="Flight"
        )

    @property
    def next_flight(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.BUTTON,
            name="Next Flight"
        )

    @property
    def next(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.BUTTON,
            name="Continue"
        )
