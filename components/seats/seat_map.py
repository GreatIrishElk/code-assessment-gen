import string

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button, Text
from utils import Selector


class _SeatPromptSelectors:
    MESSAGE = (By.CLASS_NAME, "seats-prompt__title")
    SUB_TEXT = (By.CLASS_NAME, "seats-prompt__sub-text")
    PRICE = (By.CLASS_NAME, "seats-prompt__amount")
    BUTTONS_CONTAINER = (By.CLASS_NAME, "seats-prompt__buttons")
    BUTTONS = (By.TAG_NAME, "button")


class _SeatRowSelectors:
    SEAT = (By.CLASS_NAME, "seatmap__seat")
    SEAR_EXTRA_LEG_ROOM = (By.CLASS_NAME, "seatmap__seat--extraleg")
    SEAT_UNDEFINED = (By.CLASS_NAME, "seatmap__seat--undefined")
    SEAT_UNAVAILABLE = (By.CLASS_NAME, "seatmap__seat--unavailable")
    SEAT_SELECTED = (By.CLASS_NAME, "seatmap__seat--selected")
    AISLE = (By.CLASS_NAME, "seatmap__seat--aisle")


class _SeatsMapSelectors:
    SEAT_COLUMNS = (By.CLASS_NAME, "seatmap__column")
    SEAT_COLUMNS_ITEM = (By.CLASS_NAME, "seatmap__column-item")
    SEAT_ROW = (By.CLASS_NAME, "seatmap__seats")
    SEAT_PROMPT = (By.CLASS_NAME, "seats-prompt__wrapper")


class _SeatPrompt(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_SeatPrompt, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _SeatPromptSelectors()

    @property
    def _messages(self) -> [WebElement]:
        return self.element.find_elements(*self._selectors.MESSAGE)

    @property
    def title(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._messages[0],
            name="Title"
        )

    @property
    def message(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._messages[1],
            name="Message"
        )

    @property
    def price(self) -> Text:
        return Text(
            parent=self.element,
            selector=self._selectors.PRICE,
            name="Price"
        )

    @property
    def _buttons_container(self) -> WebElement:
        return self.element.find_element(*self._selectors.BUTTONS_CONTAINER)

    @property
    def _buttons(self) -> [WebElement]:
        return self._buttons_container.find_elements(*self._selectors.BUTTONS)

    @property
    def no_thanks(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._buttons[0],
            name="No, Thanks"
        )

    @property
    def pick_these_seats(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._buttons[1],
            name="Pick these seats"
        )


class _SeatRow(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(_SeatRow, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _SeatRowSelectors()
        self.name = f"Row {self.row_number}"

    @property
    def row_number(self) -> int | None:
        try:
            return int(self.element.find_element(*self._selectors.AISLE).text)
        except ValueError:
            return None

    @property
    def seats(self) -> [Button]:
        seats = []
        for seat in self.element.find_elements(*self._selectors.SEAT):
            seats.append(
                Button(
                    parent=self.element,
                    selector=seat,
                    name="Seat"
                )
            )
        return seats

    @property
    def available_seats(self) -> [Button]:
        available_seats = []
        for seat in self.seats:
            if self._seat_available(seat):
                available_seats.append(seat)
        return available_seats

    @property
    def leg_room_seats(self) -> [Button]:
        leg_room = []
        for seat in self.available_seats:
            if self._selectors.SEAR_EXTRA_LEG_ROOM in seat.element.get_attribute("class"):
                leg_room.append(seat)
        return leg_room

    def _seat_available(self, seat: Button) -> bool:
        available = True
        if self._selectors.SEAT_UNDEFINED[1] in seat.element.get_attribute("class"):
            available = False
        if self._selectors.SEAT_UNAVAILABLE[1] in seat.element.get_attribute("class"):
            available = False
        if self._selectors.AISLE[1] in seat.element.get_attribute("class"):
            available = False
        if self._selectors.SEAT_SELECTED[1] in seat.element.get_attribute("class"):
            available = False
        return available

    def select_by_column(self, value: str):
        seat = self.seats[string.ascii_uppercase.index(value.upper())]
        if self._seat_available(seat):
            self._parent.parent.execute_script("arguments[0].scrollIntoView(true);", seat.element)
            seat.click()
        raise NoSuchElementException("Seat Unavailable")


class SeatsMap(BaseComponent):

    def __init__(self, parent: WebDriver | WebElement, selector: Selector | WebElement, name: str):
        super(SeatsMap, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _SeatsMapSelectors()

    @property
    def _seat_columns(self) -> WebElement:
        return self.element.find_element(*self._selectors.SEAT_COLUMNS)

    @property
    def seat_columns_items(self) -> [Text]:
        items = []
        for item in self._seat_columns.find_elements(self._selectors.SEAT_COLUMNS_ITEM):
            items.append(
                Text(
                    parent=self.element,
                    selector=item,
                    name=f"Row {item.text}"
                )
            )
        return items

    @property
    def seat_rows(self) -> [_SeatRow]:
        rows = []
        for row in self.element.find_elements(*self._selectors.SEAT_ROW):
            rows.append(
                _SeatRow(
                    parent=self.element,
                    selector=row,
                    name="Row"
                )
            )
        return rows

    @property
    def seats_prompt(self) -> _SeatPrompt:
        return _SeatPrompt(
            parent=self.element,
            selector=self._selectors.SEAT_PROMPT,
            name="Seat Prompt"
        )

    def get_row(self, value: int) -> _SeatRow:
        for row in self.seat_rows:
            if row.row_number == value:
                return row
        raise NoSuchElementException
