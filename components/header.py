from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components.simple_components import BaseComponent, Button
from utils import Selector


class _Selectors:
    RYANAIR_LOGO = (By.CLASS_NAME, "hp-header__logo-container")


class Header(BaseComponent):

    def __init__(self, parent: WebDriver, selector: Selector | WebElement, name: str):
        super(Header, self).__init__(parent=parent, selector=selector, name=name)
        self._selectors = _Selectors()

    @property
    def title(self) -> Button:
        return Button(
            parent=self.element,
            selector=self._selectors.RYANAIR_LOGO,
            name="Logo"
        )

    @property
    def size(self) -> dict:
        return self.element.size
