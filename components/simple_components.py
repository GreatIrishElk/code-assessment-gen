from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components import BaseComponent
from utils import Selector
from utils.logger import logger


class Text(BaseComponent):

    @property
    def value(self):
        logger.info("Getting text for text %s", self._name)
        return self.element.text

    def click(self):
        logger.info("Clicking text %s", self._name)
        self.element.click()


class TextField(BaseComponent):

    @property
    def value(self) -> str:
        logger.info("Getting text for textfield %s", self._name)
        return self.element.text

    @value.setter
    def value(self, value):
        logger.info("Setting text for textfield %s", self._name)
        self.element.clear()
        self.element.send_keys(value)

    @property
    def label(self):
        logger.info("Getting label for textfield %s", self._name)
        return self.element.get_attribute("label")

    @property
    def enabled(self):
        logger.info("Checking if textfield %s is enabled", self._name)
        return self.element.is_enabled()

    def clear(self):
        logger.info("Clearing textfield %s", self._name)
        self.element.clear()

    def click(self):
        logger.info("Clicking textfield %s", self._name)
        self.element.click()


class Button(BaseComponent):

    @property
    def value(self) -> str:
        logger.info("Setting text for button %s", self._name)
        return self.element.text

    def click(self):
        logger.info("Clicking button %s", self._name)
        self.element.click()


class Image(BaseComponent):
    def click(self):
        logger.info("Clicking image %s", self._name)
        return self.element.click()


class Link(Text):
    @property
    def url(self):
        return self.element.get_attribute("href")

    def press(self):
        self.element.click()


class _DropdownList(BaseComponent):

    def __init__(
            self,
            parent: WebDriver | WebElement,
            selector: Selector | WebElement,
            name: str,
            item_selector: Selector
    ):
        super(_DropdownList, self).__init__(parent=parent, selector=selector, name=name)
        self._item_selector = item_selector

    @property
    def dropdown_items(self) -> [Text]:
        items = []
        for item in self.element.find_elements(*self._item_selector):
            items.append(
                Text(
                    parent=self.element,
                    selector=item,
                    name=item.text
                )
            )
        return items


class DropdownMenu(Button):

    def __init__(
            self,
            parent: WebDriver | WebElement,
            selector: Selector | WebElement,
            name: str,
            dropdown_list: Selector | WebElement,
            dropdown_item: Selector | WebElement
    ):
        super(DropdownMenu, self).__init__(parent=parent, selector=selector, name=name)
        self._dropdown_list = dropdown_list
        self._dropdown_item = dropdown_item

    @property
    def dropdown_list(self) -> _DropdownList:
        return _DropdownList(
            parent=self.element,
            selector=self._dropdown_list,
            name="Dropdown List",
            item_selector=self._dropdown_item
        )

    @dropdown_list.setter
    def dropdown_list(self, value):
        for item in self.dropdown_list.dropdown_items:
            if item.value == value:
                item.click()
                break
        else:
            raise NoSuchElementException

    def set(self, value):
        if self.dropdown_list.displayed:
            self.dropdown_list = value
        self.click()
        self.dropdown_list = value


class ToggleButton(BaseComponent):
    @property
    def on(self) -> bool:
        logger.info("Getting value for toggle button %s", self._name)
        return self.element.get_attribute("value") == "1"

    @on.setter
    def on(self, value: bool):
        logger.info("Setting %s toggle button to %s", self._name, value)
        if self.on != value:
            self.click()

    def click(self):
        logger.info("Clicking toggle button %s", self._name)
        return self.element.click()
