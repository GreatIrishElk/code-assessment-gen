import time

from selenium.webdriver.remote.webdriver import WebDriver

from utils.config import config


def wait_for_url(driver: WebDriver, url: str, wait: int = config.wait):
    waited = 0
    while url not in driver.current_url:
        if waited > wait:
            break
        time.sleep(1)
        waited += 1
