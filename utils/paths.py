from pathlib import Path


def get_root_dir() -> Path:
    """
    Finds root directory of project using the pytest.ini file.
    :return: Path - the path to the root of the project.
    """
    current = Path().resolve()
    for search_dir in [current.parent, current]:
        if (search_dir / "pytest.ini").exists():
            return search_dir
    raise RuntimeError("Cannot find pytest.ini in root directory.")


root = get_root_dir()
