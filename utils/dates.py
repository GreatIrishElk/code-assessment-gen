from datetime import datetime


def string_to_timestamp(day, month, year) -> datetime:
    return datetime.strptime(f"{day}/{month}/{year}", "%d/%B/%Y")


def short_date(timestamp: datetime) -> str:
    try:
        return timestamp.strftime("%-d %b")
    except ValueError:
        return timestamp.strftime("%#d %b")
