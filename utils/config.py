import json
from pathlib import Path

from utils.paths import root


class Config:

    def __init__(self, config_name: str = "config.json"):
        self._config_name = config_name
        self._config_path = self._get_config_path() / self._config_name
        self.config = self._read_config()

    def _read_config(self) -> dict:
        if self._config_path.exists():
            with self._config_path.open() as conf:
                return json.load(conf)
        raise FileNotFoundError(f"No config file found with name: {self._config_path}")

    def _get_config_path(self) -> Path:
        current = Path().resolve()
        for search_path in [current.parent, current]:
            if (search_path / self._config_name).exists():
                return search_path
        raise FileNotFoundError(f"No config file found with name: {self._config_name}")

    @property
    def capabilities(self) -> dict:
        caps = self.config.get("capabilities")
        if caps:
            return caps
        raise KeyError("capabilities are not defined in %s", self._config_path)

    @property
    def executor(self) -> str:
        return self.config.get("executor")

    @property
    def wait(self) -> int:
        return int(self.config.get("wait", 2))

    @property
    def output(self) -> Path | None:
        try:
            output = Path(self.config.get("output", "output"))
            if not output.is_absolute():
                output = root / output
            output.mkdir(exist_ok=True)
            return output
        except TypeError:
            return None

    @property
    def logging(self):
        class Logging:
            def __init__(self, logging: dict):
                self.directory = logging.get("directory", "logs/")
                self.filename = logging.get("file", "test-logs.log")
                self.appending = logging.get("appending", False)
                self.level = logging.get("level", "DEBUG")

        return Logging(self.config.get("logging"))


config = Config()


class Capabilities:

    def __init__(self):
        self._conf = config
        self._capabilities = self._conf.capabilities

    @property
    def browser_nane(self) -> str:
        return self._capabilities.get("browserName")

    @property
    def capabilities(self) -> dict:
        return self._capabilities


capabilities = Capabilities()
