from selenium import webdriver
from selenium.webdriver.common.options import ArgOptions

from utils.config import capabilities as caps
from utils.config import config
from utils.logger import logger


def _parse_options(capabilities: dict) -> ArgOptions:
    options = ArgOptions()
    for key, value in capabilities.items():
        options.set_capability(key, value)
    return options


def get_webdriver() -> webdriver.Remote:
    logger.info("Getting selenium driver")
    driver = webdriver.Remote(
        command_executor=config.executor,
        options=_parse_options(caps.capabilities)
    )
    driver.implicitly_wait(config.wait)
    driver.maximize_window()
    return driver
