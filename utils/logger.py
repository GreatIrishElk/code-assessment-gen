import logging

from utils.config import config
from utils.paths import get_root_dir

MODE = "w"

logger = logging.getLogger(__name__)
logger.setLevel(config.logging.level)
formatter = logging.Formatter(
    "%(asctime)s - (%(filename)s:%(lineno)s) [%(levelname)s]  %(message)s"
)
directory = get_root_dir() / config.output / config.logging.directory

directory.mkdir(exist_ok=True)
filename = directory / config.logging.filename
handler = logging.FileHandler(filename, mode=MODE, encoding="UTF-8")
handler.setFormatter(formatter)
logger.addHandler(handler)
