from selenium.webdriver.common.by import By

from components import Header
from components.home import (DatePickerFrom, DatePickerTo, DestinationSelector,
                             OriginSelector, PassengersSelector, SearchWidget)
from page_object_models.base_page import BasePage


class _Selectors:
    HEADER = (By.CLASS_NAME, "header--desktop ")
    SEARCH_WIDGET = (By.CLASS_NAME, "search-widget__content")
    TOOL_TIP = (By.CLASS_NAME, "tooltip-inner")
    DATE_PICKER = (By.TAG_NAME, "fsw-flexible-datepicker-container")
    PASSENGER_PICKER = (By.TAG_NAME, "fsw-passengers-picker")


class Home(BasePage):
    def __init__(self, driver):
        super(Home, self).__init__(driver=driver)
        self._selectors = _Selectors()

    @property
    def page_name(self) -> str:
        return "Home"

    @property
    def header(self) -> Header:
        return Header(
            parent=self._driver,
            selector=self._selectors.HEADER,
            name="Header"
        )

    @property
    def search_widget(self) -> SearchWidget:
        return SearchWidget(
            parent=self._driver,
            selector=self._selectors.SEARCH_WIDGET,
            name="Search Widget"
        )

    @property
    def origin_selector(self) -> OriginSelector:
        return OriginSelector(
            parent=self._driver,
            selector=self._selectors.TOOL_TIP,
            name="Origin Selector"
        )

    @property
    def destination_selector(self) -> DestinationSelector:
        return DestinationSelector(
            parent=self._driver,
            selector=self._selectors.TOOL_TIP,
            name="Destination Selector"
        )

    @property
    def date_selector_from(self) -> DatePickerFrom:
        return DatePickerFrom(
            parent=self._driver,
            selector=self._selectors.DATE_PICKER,
            name="Date Selector"
        )

    @property
    def date_selector_to(self) -> DatePickerTo:
        return DatePickerTo(
            parent=self._driver,
            selector=self._selectors.DATE_PICKER,
            name="Date Selector"
        )

    @property
    def passenger_selector(self) -> PassengersSelector:
        return PassengersSelector(
            parent=self._driver,
            selector=self._selectors.PASSENGER_PICKER,
            name="Passenger Selector"
        )
