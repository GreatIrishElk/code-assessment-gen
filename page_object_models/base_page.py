import abc
from abc import ABC

from selenium.webdriver.remote.webdriver import WebDriver


class BasePage(ABC):

    def __init__(self, driver: WebDriver):
        self._driver = driver

    @property
    @abc.abstractmethod
    def page_name(self):
        pass

    def scroll_by(self, scroll: tuple[int, int]):
        self._driver.execute_script(f"window.scrollBy{scroll}", "")
