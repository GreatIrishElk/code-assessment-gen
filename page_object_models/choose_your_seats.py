from selenium.common.exceptions import (ElementClickInterceptedException,
                                        NoSuchElementException)
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components import FlightBreadCrumbs, Header
from components.seats import FastTrackPopUp, SeatsInfo, SeatsMap
from components.simple_components import Text
from page_object_models.base_page import BasePage


class _Selectors:
    HEADER = (By.CLASS_NAME, "header--desktop ")
    FLIGHT_BREADCRUMBS = (By.CLASS_NAME, "breadcrumb")
    TITLE = (By.CLASS_NAME, "seats-page-title-wrapper")
    SEATS_CONTAINER = (By.CLASS_NAME, "seatmap-container")
    SEATS_COLUMN = (By.CLASS_NAME, "seatmap-container__column")
    PERKS = (By.TAG_NAME, "perks")
    FAST_TRACK_POPUP = (By.CLASS_NAME, "enhanced-takeover-beta__modal")


class ChooseYourSeats(BasePage):

    def __init__(self, driver: WebDriver):
        super(ChooseYourSeats, self).__init__(driver=driver)
        self._selectors = _Selectors()

    @property
    def page_name(self) -> str:
        return "Choose Your Seats"

    @property
    def header(self) -> Header:
        return Header(
            parent=self._driver,
            selector=self._selectors.HEADER,
            name="Header"
        )

    @property
    def flight_breadcrumbs(self) -> FlightBreadCrumbs:
        return FlightBreadCrumbs(
            parent=self._driver,
            selector=self._selectors.FLIGHT_BREADCRUMBS,
            name="Flight Breadcrumbs"
        )

    @property
    def title(self) -> Text:
        return Text(
            parent=self._driver,
            selector=self._selectors.TITLE,
            name="Title"
        )

    @property
    def _seats_container(self) -> WebElement:
        return self._driver.find_element(*self._selectors.SEATS_CONTAINER)

    @property
    def _seats_columns(self) -> [WebElement]:
        return self._seats_container.find_elements(*self._selectors.SEATS_COLUMN)

    @property
    def seat_map(self) -> SeatsMap:
        return SeatsMap(
            parent=self._seats_container,
            selector=self._seats_columns[0],
            name="Seat Map"
        )

    @property
    def seats_info(self) -> SeatsInfo:
        return SeatsInfo(
            parent=self._seats_container,
            selector=self._seats_columns[1],
            name="Seats Info"
        )

    @property
    def fast_track_popup(self) -> FastTrackPopUp:
        return FastTrackPopUp(
            parent=self._driver,
            selector=self._selectors.FAST_TRACK_POPUP,
            name="Fast Track Popup"
        )

    def select_row_seat(self, row: int, column: str):
        self.seat_map.get_row(row).select_by_column(column)

    def select_available_seat(self):
        for row in self.seat_map.seat_rows:
            available_seats = row.available_seats
            if available_seats:
                seat = available_seats[0]
                try:
                    seat.click()
                except ElementClickInterceptedException:
                    self.scroll_by((0, -self.header.size["height"]))
                    seat.click()
                break
        else:
            NoSuchElementException("No available seats found.")
