from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement

from components import FlightBreadCrumbs
from components.flights import (Fares, FlightDetails, Journey, LogInWidget,
                                Passengers)
from components.simple_components import Button
from page_object_models.base_page import BasePage


class _Selectors:
    TRIP_DETAILS_HEADER = (By.CLASS_NAME, "trip-details-header")
    FLIGHT_BREADCRUMBS = (By.CLASS_NAME, "breadcrumb")
    JOURNEY = (By.CLASS_NAME, "journeys-wrapper")
    OUTBOUND = (By.TAG_NAME, "div")
    INBOUND = (By.CLASS_NAME, "inbound")
    FARES = (By.TAG_NAME, "fare-selector-container")
    LOG_IN_WIDGET = (By.CLASS_NAME, "login-touchpoint")
    PASSENGERS = (By.CLASS_NAME, "form-wrapper")
    CONTINUE = (By.CLASS_NAME, "continue-flow__button")


class Trip(BasePage):

    def __init__(self, driver: WebDriver):
        super(Trip, self).__init__(driver=driver)
        self._selectors = _Selectors()

    @property
    def page_name(self) -> str:
        return "Flight Selection"

    @property
    def trip_details_header(self) -> FlightDetails:
        return FlightDetails(
            parent=self._driver,
            selector=self._selectors.TRIP_DETAILS_HEADER,
            name="Trip Details Header"
        )

    @property
    def flight_breadcrumbs(self) -> FlightBreadCrumbs:
        return FlightBreadCrumbs(
            parent=self._driver,
            selector=self._selectors.FLIGHT_BREADCRUMBS,
            name="Flight Breadcrumbs"
        )

    @property
    def _journey_wrapper(self) -> WebElement:
        return self._driver.find_element(*self._selectors.JOURNEY)

    @property
    def outbound(self) -> Journey:
        return Journey(
            parent=self._journey_wrapper,
            selector=self._journey_wrapper.find_element(*self._selectors.OUTBOUND),
            name="Outbound"
        )

    @property
    def inbound(self) -> Journey:
        return Journey(
            parent=self._journey_wrapper,
            selector=self._selectors.INBOUND,
            name="Inbound"
        )

    @property
    def fares(self) -> Fares:
        return Fares(
            parent=self._driver,
            selector=self._selectors.FARES,
            name="Names"
        )

    @property
    def log_in_widget(self) -> LogInWidget:
        return LogInWidget(
            parent=self._driver,
            selector=self._selectors.LOG_IN_WIDGET,
            name="Log In Widget"
        )

    @property
    def passengers(self) -> Passengers:
        return Passengers(
            parent=self._driver,
            selector=self._selectors.PASSENGERS,
            name="Passengers"
        )

    @property
    def continue_b(self) -> Button:
        return Button(
            parent=self._driver,
            selector=self._selectors.CONTINUE,
            name="Continue"
        )
