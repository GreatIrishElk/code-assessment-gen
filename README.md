# Coding Challenge - Gen - Simon Murphy

## Requirements

- Python 3.11
- Pip 21.3.1
- Java 11 or above
- [Selenium Grid](https://github.com/SeleniumHQ/selenium/releases/tag/selenium-4.8.0)
- Google Chrome
- [Chromedriver](https://chromedriver.chromium.org/downloads)
- selenium 4.8.2
- pytest 7.2.1

## Configuration

- OPTIONAL: Create a virtual python environment.
- Python requirements can be installed with the command `pip install -r requirements`.
- Add this repository to `PYTHONPATH` environment variable:
    - On Windows:
        - Open `System Properties`,
        - Select `Environment Variable`,
        - Select `New`, or edit an existing `PYTHONPATH` environment variable,
        - Select `Edit`,
        - Enter the path to this repository, separated by a semicolon if
        - Select `OK`,
        - Select `OK`,
        - Select `OK`.
    - On Ubuntu or MacOS:
        - Open a terminal,
        - Execute the command `export PYTHONPATH="path/to/directory:${PYTHONPATH}"`, where `path/to/directory` is the
          path
          to repositories location.
        - NOTE: The above command can also be added to your RC file, e.g. `~/.bash_profile`, `~/.bashrc`, `~/.zshrc`.
- Chromedriver must be added to the `PATH` environment variable:
    - On Windows:
        - Open `System Properties`,
        - Select `Environment Variable`,
        - Select the `Path` environment variable,
        - Select `Edit`,
        - Select `New`,
        - Enter the path to the directory where the `Chromedriver` exe is localed,
        - Select `OK`,
        - Select `OK`.
    - On Ubuntu or MacOS:
        - Open a terminal,
        - Execute the command `export PATH="path/to/directory:${PATH}"`, where `path/to/directory` is the path to
          Chromedriver's location.
        - NOTE: The above command can also be added to your RC file, e.g. `~/.bash_profile`, `~/.bashrc`, `~/.zshrc`.
          Note: The versions of Google Chrome and Chromedriver need to match, e.g. Google Chrome version 110 requires
          Chromedriver version 110.

## Execute Tests

- Start Selenium Grid:
    - Open a terminal,
    - Navigate to the location of Selenium Grid,
    - Execute the command `java -jar selenium-server-4.8.0.jar standalone`,
    - Note the IP address of the Selenium Grid.
- Execute test case:
    - Open the `config.json` file,
    - Update the IP address in the field `executor` to the IP address noted from the Selenium Grid,
    - Open another terminal/a terminal tab,
    - Navigate to the root directory of this repository,
    - OPTIONAL: Activate a virtual python environment with the `requirements.txt` requirements installed,
    - Execute the command `pytest`